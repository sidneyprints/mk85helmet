#include <VarSpeedServo.h>
#include <DFRobotDFPlayerMini.h>
#include <SoftwareSerial.h>

// Declare pin settings
const int servo1Pin = 2; // set the pin for servo 1
const int servo2Pin = 4; // set the pin for servo 2
const int buttonPin = 8; // the pin that the pushbutton is attached to

// led control pins (need to be PWM enabled pins for fading)
const int leftEyePin =  3;  // left eye LEDs
const int rightEyePin =  6;  // right eye LEDs
const int AuxLED = 7; // Aux LED non-PWM

// sound board pins
const int rx_pin = 0; // set pin for receive (RX) communications
const int tx_pin = 1; // set pin for transmit (TX) communications

// Declare servo objects
VarSpeedServo servo1; // create servo object to control servo 1
VarSpeedServo servo2; // create servo object to control servo 2

// Declare variables for servo control
const int servoCloseSpeed = 100; // set the speed of the servo close function
const int servoOpenSpeed = 255; // set the speed of the servo opening recommend set to max speed to aid in lift

const int servo1_OpenPos = 10; // set the open position of servo 1
const int servo2_OpenPos = 180; // set the open position of servo 2
const int servo1_ClosePos = 180; // set the closed position of servo 1
const int servo2_ClosePos = 10; // set the closed position of servo 2

// Declare variables for button control
int buttonState = 0; // current state of the button
int lastButtonState = 0; // previous state of the button
boolean movieblinkOnSetup = true; //Blink LEDs on setup, Sequence based on Avengers Movie
boolean movieblinkOnClose = true; //Blink LEDs on close of faceplate, Sequence based on Avengers Movie

// Declare variables for LED control
unsigned long fadeDelay = .1; //speed of the eye 'fade'
unsigned long callDelay = 10; //length to wait to start eye flicker after face plate comes down
unsigned long blinkSpeed = 60; //delay between init blink on/off
unsigned long currentPWM = 0; // keep track of where the current PWM level is at
boolean isOpen = true; // keep track of whether or not the faceplate is open

// Declare variables for sound control
const int volume = 30; // sound board volume level (30 is max)
#define SND_CLOSE 1 // sound track for helmet closing sound
#define SND_JARVIS 2 // sound track for JARVIS sound
#define SND_OPEN 3 // sound track for helmet opening sound
SoftwareSerial serialObj(rx_pin, tx_pin); // Create object for serial communications
DFRobotDFPlayerMini mp3Obj; // Create object for DFPlayer Mini

#define S_IDLE 1
#define S_LEDON 2
#define S_WAITON 3
#define S_LEDOFF 4
#define S_WAITOFF 5
#define S_INITON 6
#define S_INITWAIT 7
#define S_MOVIEBLINK 8
#define S_SERVOUP 9
#define S_SERVODOWN 0
#define S_SERVOWAIT 10

static int state = S_LEDON; // initial state is 1, the "idle" state.
static unsigned long lastTime;  // To store the "current" time in for delays.

void simDelay(long period){
  long delayMillis = millis() + period;
  while (millis() <= delayMillis)
  {
    int x = 0; // dummy variable, does nothing
  }
}

void movieblink(){
  Serial.println("Start Movie Blink..");

  simDelay(300);

  int lowValue = 21;
  int delayInterval[] = { 210, 126, 84 };
  int delayVal = 0;

  // First blink on
  for (int i = 0; i <= lowValue; i++){
    analogWrite(rightEyePin, i);
    analogWrite(leftEyePin, i);
    digitalWrite(AuxLED, LOW);
    delayVal = delayInterval[0]/lowValue;
    simDelay(delayVal);
  }

  // Turn off
  analogWrite(rightEyePin, 0);
  analogWrite(leftEyePin, 0);
  digitalWrite(AuxLED, HIGH);
  simDelay(delayInterval[0]);

  // Second blink on
  for (int i = 0; i <= lowValue; i++){
    analogWrite(rightEyePin, i);
    analogWrite(leftEyePin, i);
    digitalWrite(AuxLED, LOW);
    delayVal = delayInterval[1]/lowValue;
    simDelay(delayVal);
  }

  // Turn off
  analogWrite(rightEyePin, 0);
  analogWrite(leftEyePin, 0);
  digitalWrite(AuxLED, HIGH);
  simDelay(delayInterval[1]);

  // Third blink on
  analogWrite(rightEyePin, lowValue);
  analogWrite(leftEyePin, lowValue);
  digitalWrite(AuxLED, LOW);
  simDelay(delayInterval[2]);

  // Turn off
  analogWrite(rightEyePin, 0);
  analogWrite(leftEyePin, 0);
  digitalWrite(AuxLED, HIGH);
  simDelay(delayInterval[2]);

  // All on
  analogWrite(rightEyePin, 255);
  analogWrite(leftEyePin, 255);
  digitalWrite(AuxLED, LOW);
  
  state = S_LEDON;    
}

void init_player(){
  serialObj.begin(9600);
  mp3Obj.begin(serialObj);
  
  Serial.println("Setting volume");
  mp3Obj.volume(volume);
  delay(1000);
 }

void setup() {
  // Set up serial port
  Serial.begin(115200);  
  Serial.print("Initial State: ");
  Serial.println(state);

  //init_player(); // initializes the sound player

  servo1.attach(servo1Pin); // attaches the servo on pin 9 to the servo object
  servo2.attach(servo2Pin); // attaches the 2nd servo on pin 10 to the servo object

  servo1.write(servo1_ClosePos, servoCloseSpeed); // sets intial position for 1st servo
  servo2.write(servo2_ClosePos, servoCloseSpeed); // sets intial position for 2nd servo

  simDelay(1000); // wait for servos to complete range of motion
  
  // Detach so motors don't "idle"
  servo1.detach();
  //servo2.detach();  

  pinMode(buttonPin, INPUT); // initialize the button pin as a input
  digitalWrite(buttonPin, HIGH); //use interal pull up resistors
  pinMode(AuxLED, OUTPUT); // set output for AUX LED
  
  //start with blinking LEDs if flag is turned on
  if( movieblinkOnSetup )
  {
    Serial.print("Blink on Setup Running");
    movieblink(); // Call the method to simulate Avengers Movie Blink Sequence
  }
}
void loop() { 
  
  switch(state)
  {
  case S_IDLE:
    // We don't need to do anything here, waiting for a forced state change, like a button press.
    //check main button state
    buttonState = digitalRead(buttonPin);

    // compare buttonState to previous state  
    if (buttonState != lastButtonState) {
      //if button pressed/down
      if (buttonState == LOW){
        //ie: pressed   
        if(isOpen == true){
          Serial.print("CLOSING FACE PLATE: ");
          Serial.println(isOpen, DEC); 
          state = S_SERVODOWN;
        }
        else{
          Serial.print("OPENING FACE PLATE: ");
          Serial.println(isOpen, DEC); 
          state = S_SERVOUP;
          state = S_LEDOFF;
        }
        isOpen = !isOpen;
      } 
      else{
        //went from ON/HIGH to LOW/OFF..ie: released
        //Serial.print("RELEASE: ");
        //Serial.println(isOpen, DEC); 
      } 
    }
    //save the current state for next loop
    lastButtonState = buttonState;
    break;

  case S_MOVIEBLINK:
    Serial.println("Movie Blink State");   
    movieblink(); // Call the method to simulate the eyes blinking on
    break;

  case S_LEDON:
    //Serial.println("Increase");    
    lastTime = millis();  // Remember the current time
    analogWrite(leftEyePin, currentPWM); // Turn on Left LED
    analogWrite(rightEyePin, currentPWM); // Turn on Right LED
    digitalWrite(AuxLED, HIGH); // Turn on AUX LED
    state = S_WAITON;  // Move to the next state
    break;

  case S_WAITON:
    // If one second has passed, then move on to the next state.
    if(millis() > (lastTime + fadeDelay)){
      if(currentPWM < 255){
        currentPWM += 5;
        state = S_LEDON;        
      }
      else{
        Serial.println("@ 255 done");
        state = S_IDLE;
        //state = S_LEDOFF; //no auto turn off.. set to idle state
      }
    }
    break;

  case S_LEDOFF:
    //Serial.println("........decrease");     
    lastTime = millis();  // Remember the current time
    analogWrite(leftEyePin, currentPWM); // Turn off Left LED
    analogWrite(rightEyePin, currentPWM); // Turn off Right LED
    digitalWrite(AuxLED, LOW); // Turn off AUX LED
    state = S_WAITOFF;
    break;

  case S_WAITOFF:
    // If one second has passed, then move on to the next state.
    if(millis() > (lastTime + fadeDelay)){
      if(currentPWM > 0){  //change 0 to higher number to init face 'up' function sooner.
        currentPWM -= 5;
        state = S_LEDOFF;        
      }
      else{
        Serial.println("Done");
        state = S_SERVOUP; //leds off..raise faceplate
      }
    }
    break; 

  case S_SERVOUP:
    lastTime = millis();  // Remember the current time
    Serial.println("Servo Up"); 

    // Play sound effect for helmet closing
    //mp3Obj.playMp3Folder(SND_OPEN);

    // Re-attach the servos to their pins
    servo1.attach(servo1Pin);
    servo2.attach(servo2Pin);

    // Play sound effect for helmet opening
    //mp3Obj.playMp3Folder(SND_OPEN);

    // Send data to the servos for movement
    servo1.write(servo1_OpenPos, servoOpenSpeed);
    servo2.write(servo2_OpenPos, servoOpenSpeed);

    simDelay(1000); // wait doesn't wait long enough for servos to fully complete...

    // Detach so motors don't "idle"
    servo1.detach();
    //servo2.detach();

    state = S_IDLE;    
    break;

  case S_SERVODOWN:
    lastTime = millis();  // Remember the current time
    Serial.println("Servo Down");  

    // Re-attach the servos to their pins
    servo1.attach(servo1Pin);
    servo2.attach(servo2Pin);

    // Send data to the servos for movement 
    servo1.write(servo1_ClosePos, servoCloseSpeed);
    servo2.write(servo2_ClosePos, servoCloseSpeed);

    // Play sound effect for helmet closing
    //mp3Obj.playMp3Folder(SND_CLOSE);

    simDelay(1000); // wait doesn't wait long enough for servos to fully complete...

    // Detach so motors don't "idle"
    servo1.detach();
    servo2.detach();

    if( movieblinkOnClose ) {
      Serial.println("Blink on Close Running");
      movieblink(); // Call the method to simulate Avengers Movie Blink Sequence
    }

    // Play sound effect for JARVIS
    //simDelay(3000); // pause for effect...
    //mp3Obj.playMp3Folder(SND_JARVIS);
    
    state = S_LEDON;    
    break;

  case S_SERVOWAIT:
    // If enough time has passed, call the eye flicker routine
    if(millis() > (lastTime + callDelay)){   
      Serial.println("start eye routine");
      state = S_LEDON;        
    }
    else{
      Serial.println("Waiting");
    }
    break;

  default:
    state = S_IDLE;
    break;
  } 
}